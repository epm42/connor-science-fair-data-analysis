import numpy as np
import pandas as pd
import os


def Connor_Data_to_np(filePath, colIndex: int, rowIndex: int,
                      skipRows, useCols):
    df = pd.read_excel(filePath, index_col=colIndex, header=rowIndex,
                       skiprows=skipRows, usecols=useCols)
    return df.to_numpy(na_value=-42069)


# oh_xlsx = os.path.join(os.path.expanduser('~'), 'pyEnvs', 'connorData',
#                        'spreadsheets', 'Oh.xlsx')
# oh = Connor_Data_to_np(oh_xlsx, 0, 12, [35, 36, 37], 'A, J, AD')
#
# data1_path = os.path.join(os.path.expanduser('~'), 'pyEnvs', 'connorData',
#                           'data', 'data1.txt')
#
# np.savetxt(data1_path, oh)

ConnorSheet2_xlsx = os.path.join(os.path.expanduser('~'), 'pyEnvs',
                                 'connorData', 'spreadsheets',
                                 'ConnorSheet2.xlsx')

ConnorSheet2 = Connor_Data_to_np(ConnorSheet2_xlsx, 0, 3, [39],
                                 'A, J, M, P, Q, Y, AD')

ConnorSheet2_path = os.path.join(os.path.expanduser('~'), 'pyEnvs',
                                 'connorData', 'data',
                                 'GSW_E_VPleaf_VPDleaf_Tleaf_Qamb.txt')

print(ConnorSheet2)

np.savetxt(ConnorSheet2_path, ConnorSheet2)
