import numpy as np
# from numpy.polynomial.polynomial import Polynomial as poly
import matplotlib.pyplot as plt
import os


def Read_Array_Data(fileName: str):  # Read and return array from file
    return np.loadtxt(os.path.join(os.path.expanduser('~'), 'pyEnvs',
                      'connorData', 'data', fileName))


def Split_Array(raw, keyArg):  # Split array in two based on a keyword
    for i in range(np.shape(raw)[0]):
        if raw[i][0] == (keyArg / 1):
            limitingIndex = i

    A = np.empty([limitingIndex, 6])
    B = np.empty([np.shape(raw)[0] - (limitingIndex + 1), 6])

    for i in range(np.shape(raw)[0]):
        if i < limitingIndex:
            A[i] = raw[i]
        elif i > limitingIndex:
            B[i - (limitingIndex + 1)] = raw[i]
        else:
            pass

    return [A, B]


# Returns an array for the regression line
def Regression(dataSet, axis1, axis2):

    dataSetModel = np.poly1d(np.polyfit(dataSet[axis1], dataSet[axis2], 1))
    dataSim = np.linspace(np.amin(dataSet[axis1]), np.amax(dataSet[axis1]), 10)
    dataModel = dataSetModel(dataSim)  # dataSetModel(dataSim)
    return [dataSim, dataModel]


def Set_Neg_to_Zero(dataSet, index):
    for i in range(np.size(dataSet[index])):
        if dataSet[index][i] < 0:
            dataSet[index][i] = 0
    return dataSet


# Variables to control output
# Axes
Qamb = 0
Temp = 1
VPD = 2
VP = 3
E = 4
GSW = 5

# X and Y
X = 0
Y = GSW

# Regression on/off
regressionDisplay = True

rawDat = Read_Array_Data('GSW_E_VPleaf_VPDleaf_Tleaf_Qamb.txt')
temp = Split_Array(rawDat, -42069)

host = np.rot90(temp[0], k=1)
pHost = np.rot90(temp[1], k=1)

# Set negative GSW values to 0
host = Set_Neg_to_Zero(host, GSW)
pHost = Set_Neg_to_Zero(pHost, GSW)

# Set negative E values to 0
host = Set_Neg_to_Zero(host, E)
pHost = Set_Neg_to_Zero(pHost, E)

hostAvg = [np.average(host[X]), np.average(host[Y])]
pHostAvg = [np.average(pHost[X]), np.average(pHost[Y])]

hostRegression = Regression(host, X, Y)
pHostRegression = Regression(pHost, X, Y)

# print(pHostAvg)

# print(host[Y])

fig, ((ax, ax1), (ax2, ax3)) = plt.subplots(2, 2)

fig.set_size_inches(12, 9)

# ------------------Parasitized-and-Unparasitized---------------------

# Plot raw data
ax.plot(host[X], host[Y], 'bo', label='Unparasitized')
ax.plot(pHost[X], pHost[Y], 'o', c='orange', label='Parasitized')

# Plot the average value of the data
ax.plot(hostAvg[0], hostAvg[1], 'go', label='Unparasitized Avg')
ax.plot(pHostAvg[0], pHostAvg[1], 'ro', label='Parasitized Avg')

# Plot the regression lines for the data
if regressionDisplay:
    ax.plot(hostRegression[0], hostRegression[1],
            c='blue', label='Unparasitized')
    ax.plot(pHostRegression[0], pHostRegression[1],
            c='orange', label='Parasitized')
else:
    pass

# ------------------Only-Unparasitized---------------------

# Plot raw data
ax1.plot(host[X], host[Y], 'bo', label='Unparasitized')

# Plot the average value of the data
ax1.plot(hostAvg[0], hostAvg[1], 'go', label='Unparasitized Avg')

if regressionDisplay:
    ax1.plot(hostRegression[0], hostRegression[1],
             c='blue', label='Unparasitized')
else:
    pass

# ------------------Parasitized---------------------

# Plot raw data
ax2.plot(pHost[X], pHost[Y], 'o', c='orange', label='Parasitized')

# Plot the average value of the data
ax2.plot(pHostAvg[0], pHostAvg[1], 'ro', label='Parasitized Avg')

# Plot the regression lines for the data
if regressionDisplay:
    ax2.plot(pHostRegression[0], pHostRegression[1],
             c='orange', label='Parasitized')
else:
    pass

# ------------------Only-regression---------------------

# Plot the regression lines for the data
if regressionDisplay:
    ax3.plot(hostRegression[0], hostRegression[1],
             c='blue', label='Unparasitized')
    ax3.plot(pHostRegression[0], pHostRegression[1],
             c='orange', label='Parasitized')
else:
    pass

if X == Qamb:
    xLabel = 'Qamb'
elif X == Temp:
    xLabel = 'Temp'
elif X == VPD:
    xLabel = 'VPD'
elif X == VP:
    xLabel = 'VP'
elif X == E:
    xLabel = 'E'
elif X == GSW:
    xLabel = 'GSW'

if Y == Qamb:
    yLabel = 'Qamb'
elif Y == Temp:
    yLabel = 'Temp'
elif Y == VPD:
    yLabel = 'VPD'
elif Y == VP:
    yLabel = 'VP'
elif Y == E:
    yLabel = 'E'
elif Y == GSW:
    yLabel = 'GSW'

ax2.set_xlabel(xLabel, fontsize=16)
ax3.set_xlabel(xLabel, fontsize=16)
ax.set_ylabel(yLabel, fontsize=16)
ax2.set_ylabel(yLabel, fontsize=16)

fig.suptitle('{} vs. {}'.format(yLabel, xLabel), fontsize=20)

ax.legend()

# ax.grid(linestyle='-')
# ax1.grid(linestyle='-')
# ax2.grid(linestyle='-')
# ax3.grid(linestyle='-')

# ax.set_xscale('log')
# ax1.set_xscale('log')
# ax2.set_xscale('log')
# ax3.set_xscale('log')

plt.tight_layout()

plt.savefig(os.path.join(os.path.expanduser('~'), 'pyEnvs',
            'connorData', 'pics', '{}_vs_{}.png'.format(yLabel, xLabel)),
            dpi=200, facecolor='w')
plt.show()
